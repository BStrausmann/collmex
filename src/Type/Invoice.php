<?php
/**
 * Collmex Invoice Type
 *
 * @author    Marcus Jaschen <mail@marcusjaschen.de>
 * @license   http://www.opensource.org/licenses/mit-license MIT License
 * @link      https://github.com/mjaschen/collmex
 */

namespace MarcusJaschen\Collmex\Type;

/**
 * Collmex Invoice Type
 *
 * @author   Marcus Jaschen <mail@marcusjaschen.de>
 * @license  http://www.opensource.org/licenses/mit-license MIT License
 * @link     https://github.com/mjaschen/collmex
 */
class Invoice extends AbstractType implements TypeInterface
{
    const INVOICE_TYPE_INVOICE            = 0;
    const INVOICE_TYPE_CREDIT_MEMO        = 1;
    const INVOICE_TYPE_DOWN_PAYMENT       = 2;
    const INVOICE_TYPE_CASH_SALE          = 3;
    const INVOICE_TYPE_CREDIT_FOR_RETURNS = 4;
    const INVOICE_TYPE_PRO_FORMA_INVOICE  = 5;

    const NOT_DELETED = 0;
    const DELETED     = 1;

    const LANGUAGE_GERMAN  = 0;
    const LANGUAGE_ENGLISH = 1;

    const STATUS_NEW      = 0;
    const STATUS_TO_BOOK  = 10;
    const STATUS_OPEN     = 20;
    const STATUS_REMINDED = 30;
    const STATUS_DONE     = 40;
    const STATUS_DELETED  = 100;

    const POSITION_NORMAL = 0;
    const POSITION_SUM    = 1;
    const POSITION_TEXT   = 2;
    const POSITION_FREE   = 3;

    const TAX_RATE_FULL    = 0;
    const TAX_RATE_REDUCED = 1;
    const TAX_RATE_TAXFREE = 2;

    /**
     * Type data template
     *
     * @var array
     */
    protected $template = array(
        'type_identifier'                => 'CMXINV',	// 1	Satzart
        'invoice_id'                     => null,		// 		Rechnungsnummer
        'position'                       => null,		//		Position
        'invoice_type'                   => null,		//		Rechnungsart
        'client_id'                      => null,		// 5	Firma Nr
        'order_id'                       => null,		//		Auftrag Nr
        'customer_id'                    => null,		//		Kunden-Nr
        'customer_salutation'            => null,		//		Kunde-Anrede
        'customer_title'                 => null,		//		Kunde-Titel
        'customer_forename'              => null,		// 10	Kunde-Vorname
        'customer_lastname'              => null,		//		Kunde-Name
        'customer_firm'                  => null,		//		Kunde-Firma
        'customer_department'            => null,		//		Kunde-Abteilung
        'customer_street'                => null,		//		Kunde-Strasse
        'customer_zipcode'               => null,		// 15	Kunde-PLZ
        'customer_city'                  => null,		//		Kunde-Ort
        'customer_country'               => null,		//		Kunde-Land
        'customer_phone'                 => null,		//		Kunde-Telefon
        'customer_phone_2'               => null,		//		Kunde-Telefon2
        'customer_fax'                   => null,		// 20	Kunde-Telefax
        'customer_email'                 => null,		//		Kunde-E-Mail
        'customer_bank_account'          => null,		//		Kunde-Kontonummer
        'customer_bank_code'             => null,		//		Kunde-Blz
        'customer_bank_account_owner'    => null,		//		Kunde-Abweichender Kontoinhaber
        'customer_bank_iban'             => null,		// 25	Kunde-IBAN
        'customer_bank_bic'              => null,		//		Kunde-BIC
        'customer_bank_name'             => null,		//		Kunde-Bank
        'customer_vat_id'                => null,		//		Kunde-USt.IdNr
        'reserved'                       => null,		//		Kunde-Privatperson
        'invoice_date'                   => null,		// 30	Rechnungsdatum
        'price_date'                     => null,		//		Preisdatum
        'terms_of_payment'               => null,		//		Zahlungsbedingung
        'currency'                       => null,		//		Währung (ISO-Codes)
        'price_group'                    => null,		//		Preisgruppe
		'discount_group'                 => null,		// 35	Rabattgruppe
		'discount_final'                 => null,		//		Schluss-Rabatt
        'discount_reason'                => null,		//		Rabattgrund
        'invoice_text'                   => null,		//		Rechnungstext
        'final_text'                     => null,		// 		Schlusstext
        'annotation'                     => null,		// 40	Internes Memo
        'deleted'                        => null,		//		Gelöscht	0 = nicht gelöscht, 1 = gelöscht.
        'language'                       => null,		//		Sprache		0 = Deutsch, 1 = Englisch.
        'employee_id'                    => null,		//		Bearbeiter
        'agent_id'                       => null,		//		Vermittler
        'system_name'                    => null,		// 45	Systemname
        'status'                         => null,		//		Status 		Für Export: 0 = Neu, 10 = Zu buchen, 20 = Offen, 30 = Gemahnt, 40 = Erledigt, 100 = Gelöscht
        'discount_final_2'               => null,		//		Schluss-Rabatt-2
        'discount_final_2_reason'        => null,		//		Schluss-Rabatt-2-Grund
        'shipping_id'                    => null,		//		Versandart
        'shipping_costs'                 => null,		// 50	Versandkosten
        'cod_costs'                      => null,		//		Nachnahmegebühr
        'time_of_delivery'               => null,		//		Liefer/Leistungsdatum
        'delivery_conditions'            => null,		//		Lieferbedingung
        'delivery_conditions_additional' => null,		//		Lieferbedingung Zusatz
        'delivery_salutation'            => null,		// 55	LieferAdr-Anrede
        'delivery_title'                 => null,		//		LieferAdr-Titel
        'delivery_forename'              => null,		//		LieferAdr-Vorname
        'delivery_lastname'              => null,		//		LieferAdr-Name
        'delivery_firm'                  => null,		//		LieferAdr-Firma
        'delivery_department'            => null,		// 60	LieferAdr-Abteilung
        'delivery_street'                => null,		//		LieferAdr-Strasse
        'delivery_zipcode'               => null,		//		LieferAdr-PLZ
        'delivery_city'                  => null,		//		LieferAdr-Ort
        'delivery_country'               => null,		//		LieferAdr-Land
        'delivery_phone'                 => null,		// 65	LieferAdr-Telefon
        'delivery_phone_2'               => null,		//		LieferAdr-Telefon2
        'delivery_fax'                   => null,		//		LieferAdr-Telefax
        'delivery_email'                 => null,		//		LieferAdr-E-Mail
        'position_type'                  => null,		//		Positionstyp		0 = Normalposition, 1 = Summenposition, 2 = Textposition, 3 = Kostenlos.
        'product_id'                     => null,		// 70	Produktnummer
        'product_description'            => null,		//		Produktbeschreibung
        'quantity_unit'                  => null,		//		Mengeneinheit
        'quantity'                       => null,		//		Menge
        'price'                          => null,		//		Einzelpreis
        'price_quantity'                 => null,		// 75	Preismenge
        'position_discount'              => null,		//		Positionsrabatt
        'position_value'                 => null,		//		Positionswert
        'product_type'                   => null,		//		Produktart		0 = Ware, 1 = Dienstleistung, 2 = Mitgliedsbeitrag (nur Collmex Verein), 3 = Bau/Reinigungs-Dienstleistung
        'tax_rate'                       => null,		//		Steuerklassifikation
        'foreign_tax'                    => null,		// 80	Steuer auch im Ausland
        'customer_order_position'        => null,		//		Kundenauftragsposition
        'revenue_type'                   => null,		//		Erlösart		Nur für Export. 0 = voller Steuersatz, 1 = halber Steuersatz, 2 = steuerfreies Produkt, 10 = Export, 11 = innergemeinschaftliche Lieferung, 12 = Nicht steuerbare Umsätze, 13 = Generell steuerbefreit (z.B. Kleinunternehmer), 14 = Leistungsempfänger ist steuerpflichtig
        'sum_over_positions'             => null,		//		Summe über Pos.
        'revenue'                        => null,		//		Umsatz
        'costs'                          => null,		// 85	Kosten
        'gross_profit'                   => null,		//		Rohertrag
        'margin'                         => null,		//		Marge
		'manuell_costs'                  => null,		//		Kosten manuell
    );

    /**
     * Formally validates the type data in $data attribute.
     *
     * @return bool Validation success
     */
    public function validate()
    {
        // TODO: Implement validate() method.
    }
}
