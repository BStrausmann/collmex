<?php
/**
 * Collmex Customer Type
 *
 * @author    Marcus Jaschen <mail@marcusjaschen.de>
 * @license   http://www.opensource.org/licenses/mit-license MIT License
 * @link      https://github.com/mjaschen/collmex
 */

namespace MarcusJaschen\Collmex\Type;

/**
 * Collmex Customer Type
 *
 * @author   Marcus Jaschen <mail@marcusjaschen.de>
 * @license  http://www.opensource.org/licenses/mit-license MIT License
 * @link     https://github.com/mjaschen/collmex
 */
class Customer extends AbstractType implements TypeInterface
{
    const STATUS_ACTIVE                    = 0;
    const STATUS_INACTIVE                  = 1;

    const OUTPUT_MEDIUM_PRINT              = 0;
    const OUTPUT_MEDIUM_EMAIL              = 1;
    const OUTPUT_MEDIUM_FAX                = 2;
    const OUTPUT_MEDIUM_MAIL               = 3;

    const NO_DELIVERY_BLOCK                = 0;
    const DELIVERY_BLOCK                   = 1;

    const NO_CONSTRUCTION_SERVICE_PROVIDER = 0;
    const CONSTRUCTION_SERVICE_PROVIDER    = 1;

    const LANGUAGE_GERMAN                  = 0;
    const LANGUAGE_ENGLISH                 = 1;

    /**
     * Type data template
     *
     * @var array
     */
    protected $template = array(
        'type_identifier'                => 'CMXKND',   // 1    Satzart
        'customer_id'                    => null,       //      Kundennummer
        'client_id'                      => null,       //      Firma Nr
        'salutation'                     => null,       //      Anrede
        'title'                          => null,       // 5    Title
        'forename'                       => null,       //      Vorname
        'lastname'                       => null,       //      Nachname
        'firm'                           => null,       //      Firma
        'department'                     => null,       //      Abteilung
        'street'                         => null,       // 10   Straße
        'zipcode'                        => null,       //      PLZ
        'city'                           => null,       //      Ort
        'annotation'                     => null,       //      Bemerkung
        'inactive'                       => null,       //      Inaktiv
        'country'                        => null,       // 15   Land
        'phone'                          => null,       //      Telefon
        'fax'                            => null,       //      Telefax
        'email'                          => null,       //      E-Mail
        'bank_account'                   => null,       //      Kontonummer
        'bank_code'                      => null,       // 20   BLZ
        'iban'                           => null,       //      IBAN
        'bic'                            => null,       //      BIC
        'bank_name'                      => null,       //      Bankname
        'tax_id'                         => null,       //      Steuernummer
        'vat_id'                         => null,       // 25   USt.IdNr
        'terms_of_payment'               => null,       //      Zahlungsbedingung
        'discount_id'                    => null,       //      Rabattgruppe
        'delivery_conditions'            => null,       //      Lieferbediegung
        'delivery_conditions_additional' => null,       //      Lieferbediegung Zusatz
        'output_medium'                  => null,       // 30   Ausgabemedium
        'bank_account_owner'             => null,       //      Kontoinhaber
        'address_group_id'               => null,       //      Adressgruppe
        'ebay_account_name'              => null,       //      eBay-Mitgliedsname
        'price_group_id'                 => null,       //      Preisgruppe
        'currency'                       => null,       // 35   Währung
        'agent_id'                       => null,       //      Vermittler
        'cost_center'                    => null,       //      Kostenstelle
        'follow_up_date'                 => null,       //      Wiedervorlage am
        'delivery_block'                 => null,       //      Liefersperre
        'construction_service_provider'  => null,       // 40   Bau/Reinigungs-Dienstleister
        'customer_supplier_number'       => null,       //      Lief-Nr. bei Kunden
        'output_language'                => null,       //      Ausgabesprache
        'email_cc'                       => null,       //      CC
        'phone_2'                        => null,       //      Telefon2
        'mandate_reference'              => null,       // 45   Lastschrift-Mandatsreferenz
        'mandate_reference_sign_date'    => null,       //      Datum Unterschrift
        'dunning_block'                  => null,       //      Mahnsperre
        'no_mailings'                    => null,       //      Keine Mailings
    );

    /**
     * Formally validates the type data in $data attribute.
     *
     * @return bool Validation success
     */
    public function validate()
    {
        // TODO: Implement validate() method.
    }
}
