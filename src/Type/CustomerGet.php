<?php
/**
 * Collmex Customer Get Type
 *
 * @author    Marcus Jaschen <mail@marcusjaschen.de>
 * @license   http://www.opensource.org/licenses/mit-license MIT License
 * @link      https://github.com/mjaschen/collmex
 */

namespace MarcusJaschen\Collmex\Type;

/**
 * Collmex Customer Get Type
 *
 * @author   Marcus Jaschen <mail@marcusjaschen.de>
 * @license  http://www.opensource.org/licenses/mit-license MIT License
 * @link     https://github.com/mjaschen/collmex
 */
class CustomerGet extends AbstractType implements TypeInterface
{
    /**
     * @var array
     */
    protected $template = array(
        'type_identifier'  => 'CUSTOMER_GET',	//	1	Satzart
        'customer_id'      => null,				//		Kunde Nr
        'client_id'        => null,				//		Firma Nr
        'query'            => null,				//		Text
        'follow-up'        => null,				//	5	Fällig zur Wiedervorlage
        'zipcode'          => null,				//		PLZ/Land
        'address_group_id' => null,				//		Adressgruppe
        'price_group_id'   => null,				//		Preisgruppe
        'discount_id'      => null,				//		Rabattgruppe
        'agent_id'         => null,				//	10	Vermittler
        'only_changed'     => null,				//		Nur geänderte
        'system_name'      => null,				//		Systemname
        'inactive'         => null,				//		Inaktive

    );

    /**
     * Formally validates the type data in $data attribute.
     *
     * @return bool Validation success
     */
    public function validate()
    {
        return true;
    }
}
